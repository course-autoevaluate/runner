# ##################################################################################################
#  COURSE RUNNER                                                                                   #
#  Copyright (c) 2019-2020                                                                         #
#   --------------------------------------------------------------------------                     #
#                                                                                                  #
#  This program is free software: you can redistribute it and/or modify it                         #
#  under the terms of the GNU Lesser General Public License as published by                        #
#  the Free Software Foundation, either version 3 of the License, or (at your                      #
#  option) any later version.                                                                      #
#                                                                                                  #
#  This program is distributed in the hope that it will be useful, but                             #
#  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY                      #
#  or FITNESS FOR A PARTICULAR PURPOSE.                                                            #
#  See the GNU General Public License for more details.                                            #
#                                                                                                  #
#  You should have received a copy of the GNU Lesser General Public License                        #
#  along with this program.                                                                        #
#  If not, see <https://www.gnu.org/licenses/>.                                                    #
# ##################################################################################################

from unittest import TestCase

from runner.config import Config
from runner.course import CourseApiClient


class TestAPIClient(TestCase):
    def setUp(self) -> None:
        super().setUp()
        self._config = Config()
        self.api_client = CourseApiClient(self._config)

    def test__construct_url(self):
        route = f'/api/submission/1/'
        url = self.api_client._construct_url(self._config.course_url, route=route)

        self.assertEqual(url, 'http://127.0.0.1:8001/api/submission/1/')

    def test__construct_url_with_parameter(self):
        route = f'/api/submission/1/'
        url = self.api_client._construct_url(self._config.course_url, route=route,
                                             parameters={'p': 'p1'})

        self.assertEqual(url, 'http://127.0.0.1:8001/api/submission/1/?p=p1')
