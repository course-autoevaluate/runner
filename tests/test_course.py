# ##################################################################################################
#  COURSE RUNNER                                                                                   #
#  Copyright (c) 2019-2020                                                                         #
#   --------------------------------------------------------------------------                     #
#                                                                                                  #
#  This program is free software: you can redistribute it and/or modify it                         #
#  under the terms of the GNU Lesser General Public License as published by                        #
#  the Free Software Foundation, either version 3 of the License, or (at your                      #
#  option) any later version.                                                                      #
#                                                                                                  #
#  This program is distributed in the hope that it will be useful, but                             #
#  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY                      #
#  or FITNESS FOR A PARTICULAR PURPOSE.                                                            #
#  See the GNU General Public License for more details.                                            #
#                                                                                                  #
#  You should have received a copy of the GNU Lesser General Public License                        #
#  along with this program.                                                                        #
#  If not, see <https://www.gnu.org/licenses/>.                                                    #
# ##################################################################################################

from unittest import TestCase
from unittest.mock import patch, Mock

# Third-party imports...
from nose.tools import assert_list_equal

from runner.config import Config
from runner.course import CourseApiClient


class TestCourseApiClient(TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.api_client = CourseApiClient(Config())

    def test_get_first_pending_submission_empty(self):
        mock_get_patcher = patch('runner.course.CourseApiClient.get_first_pending_submission')
        mock_get = mock_get_patcher.start()
        submissions = []

        mock_get.return_value = Mock()
        mock_get.return_value.json.return_value = submissions

        response = self.api_client.get_first_pending_submission()

        self.assertTrue(len(response.json()) == 0)

        mock_get_patcher.stop()

    def test_get_first_pending_submission_not_empty(self):
        mock_get_patcher = patch('runner.course.CourseApiClient.get_first_pending_submission')
        mock_get = mock_get_patcher.start()
        submission = {
            "id": 1,
            "file": "http://127.0.0.1:8001/uploads/submissions/raphael_7RKbjVN.cpp",
            "file_sha": "97589bd6d4707d9bb42d48215aadbe91ef7b9ae0e11820bc0e7e048da67b1ee8",
            "date_submitted": "2021-08-03T15:45:22.110223Z",
            "status": "PENDING",
            "exercise": 2,
            "language": 1,
            "by": 2,
            "runner": 1
        }

        mock_get.return_value = Mock()
        mock_get.return_value.json.return_value = submission

        response = self.api_client.get_first_pending_submission()

        self.assertIsNotNone(response.json())
        self.assertEqual(response.json(), submission)

        mock_get_patcher.stop()

    def test_get_list_of_jobs(self):
        mock_get_patcher = patch('runner.course.CourseApiClient.get_list_of_jobs')
        mock_get = mock_get_patcher.start()
        jobs = [
            {
                "id": 1,
                "status": "PENDING",
                "log": "",
                "submission": 1,
                "test_case": 1
            }]

        mock_get.return_value = Mock()
        mock_get.return_value.json.return_value = jobs

        response = self.api_client.get_list_of_jobs(1)

        assert_list_equal(response.json(), jobs)

        mock_get_patcher.stop()

    def test_get_language_information(self):
        mock_get_patcher = patch('runner.course.CourseApiClient.get_language_information')
        mock_get = mock_get_patcher.start()
        language = {
            "id": 1,
            "name": "c++",
            "allow_submit": False,
            "allow_judge": False,
            "time_factor": 1,
            "extensions": "c"
        }

        mock_get.return_value = Mock()
        mock_get.return_value.json.return_value = language

        response = self.api_client.get_language_information(1)

        self.assertIsNotNone(response.json())
        self.assertEqual(response.json(), language)

        mock_get_patcher.stop()

    def test_get_exercise(self):
        mock_get_patcher = patch('runner.course.CourseApiClient.get_exercise')
        mock_get = mock_get_patcher.start()
        ex = {
            "id": 2,
            "number": 1,
            "description": "djfklsdjflksdjflksdjflksd",
            "tp": 1,
            "executable": 1
        }

        mock_get.return_value = Mock()
        mock_get.return_value.json.return_value = ex

        response = self.api_client.get_exercise(2)

        self.assertIsNotNone(response.json())
        self.assertEqual(response.json(), ex)

        mock_get_patcher.stop()

    def test_get_executable(self):
        mock_get_patcher = patch('runner.course.CourseApiClient.get_executable')
        mock_get = mock_get_patcher.start()
        ex = {
            "id": 1,
            "name": "compare",
            "file": "http://127.0.0.1:8001/uploads/executable/compare.c",
            "build_command": "\n            #!/bin/sh\n             g++ -g -O1 -Wall -fstack-protector -D_FORTIFY_SOURCE=2 -fPIE -Wformat -Wformat-security -ansi -pedantic  -fPIE -Wl,-z,relro -Wl,-z,now  $1 -o $2\n            ",
            "file_sha": "1323eb39721c204de1ae1ac362e5a80e6746617660b0ced970ef2d06e3bf8e4a",
            "status": "COMPARE"
        }

        mock_get.return_value = Mock()
        mock_get.return_value.json.return_value = ex

        response = self.api_client.get_executable(2)

        self.assertIsNotNone(response.json())
        self.assertEqual(response.json(), ex)

        mock_get_patcher.stop()

    def test_get_test_case(self):
        mock_get_patcher = patch('runner.course.CourseApiClient.get_test_case')
        mock_get = mock_get_patcher.start()
        ex = {
                 "id": 1,
                 "sample": True,
                 "input": "http://127.0.0.1:8001/uploads/test_cases/tp_1_ex_2_sample01.in",
                 "output": "http://127.0.0.1:8001/uploads/test_cases/tp_1_ex_2_sample01.ans",
                 "input_sha": "8070eaa3d79785f01b9c62a7fd6c1e608117bbe5028b03cb4819b7882546169b",
                 "output_sha": "53c234e5e8472b6ac51c1ae1cab3fe06fad053beb8ebfd8977b010655bfdd3c3",
                 "exercise": 2
             },

        mock_get.return_value = Mock()
        mock_get.return_value.json.return_value = ex

        response = self.api_client.get_test_case(1)

        self.assertIsNotNone(response.json())
        self.assertEqual(response.json(), ex)

        mock_get_patcher.stop()

    def test_update_submission(self):
        mock_get_patcher = patch('runner.course.CourseApiClient.update_submission')
        mock_get = mock_get_patcher.start()

        mock_get.return_value = Mock()
        mock_get.return_value.json.return_value = {
            "id": 1,
            "file": "http://127.0.0.1:8001/uploads/submissions/raphael_7RKbjVN.cpp",
            "file_sha": "97589bd6d4707d9bb42d48215aadbe91ef7b9ae0e11820bc0e7e048da67b1ee8",
            "date_submitted": "2021-08-03T15:45:22.110223Z",
            "status": "OK",
            "exercise": 2,
            "language": 1,
            "by": 2,
            "runner": 1
        }

        response = self.api_client.update_submission(1, data={'status': 'PENDING'})
        self.assertIsNotNone(response)

        mock_get_patcher.stop()

    # def test_update_job(self):
    #     self.fail()
    #
    # def test_download_file(self):
    #     self.fail()
    #
    # def test_register(self):
    #     self.fail()
