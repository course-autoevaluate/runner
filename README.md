# cOUrSE - autOevaluate yoUr Student Easily - Runner

![Sonar Quality Gate](https://img.shields.io/sonar/quality_gate/course-autoevaluate_runner?server=https%3A%2F%2Fsonarcloud.io)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=course-autoevaluate_runner&metric=coverage)](https://sonarcloud.io/dashboard?id=course-autoevaluate_runner)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=course-autoevaluate_runner&metric=security_rating)](https://sonarcloud.io/dashboard?id=course-autoevaluate_runner)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=course-autoevaluate_runner&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=course-autoevaluate_runner)
[![pipeline status](https://gitlab.com/course-autoevaluate/runner/badges/master/pipeline.svg)](https://gitlab.com/course-autoevaluate/runner/-/commits/master)


This project is the runner executing the submissions from the "COURSE" tool. It is available 
[here](https://gitlab.com/course-autoevaluate/backend/).